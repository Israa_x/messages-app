from django.views.generic import ListView
from .models import Post

# Create your views here.
class HomePageView(ListView):
    model = Post #explicitly define which model we’re using. 
    template_name = "home.html"

